### Insurance Web Application
Insurance Web Application.



### Practical task:
- Using the provided skeleton, implement the REST service.
- Useful link: https://spring.io/guides/gs/rest-service
- In addition, you could use Swagger to provide API documentation.



### How to start:
1. Install the latest version of docker if you still haven't done it.
2. Run ActiveMq Docker Container in terminal using instructions below:
    - docker pull rmohr/activemq
    - docker run --name activemqmb -p 61616:61616 -p 8161:8161 rmohr/activemq
3. Run MySQL Docker Container or just use the PC client MySQL Workbench:
    - docker pull mysql
    - docker run --name mysqldb -p 3306:3306 -e MYSQL_USER=mysql -e MYSQL_PASSWORD=admin -d mysql
4. Connect to the DataBase and execute src/main/resources/script.sql.
5. Run the application and check end-points work using Postman client or this URL:
    - http://localhost:8080/swagger-ui.html



### Technologies:
- Programming language: Java 17;
- Frameworks:
    - Spring (Boot, Core, Data, WebMVC, Test);
    - Hibernate.
- Query language: SQL;
- IDE: IntelliJ IDEA;
- Database: MySQL;
- Message Broker: ActiveMQ;
- Tools: JUnit 4, Mockito, Lombok, Postman, Swagger 2, Maven, Git, Docker, JDBC, JPA, JMS, HTTPs, XML, YAML, JSON;
- Others: GitHub, CircleCI, CodeCov, SonarLint, SonarCloud.

[![CircleCI](https://dl.circleci.com/status-badge/img/circleci/X6YjqFWT2Xdu3eZJ9SoGvV/AUZmdpD1JsyX8axRqRxcGq/tree/main.svg?style=svg&circle-token=c53e8167a93214e427f99fbdc5d6762566109f92)](https://dl.circleci.com/status-badge/redirect/circleci/X6YjqFWT2Xdu3eZJ9SoGvV/AUZmdpD1JsyX8axRqRxcGq/tree/main)
[![CodeCov](https://codecov.io/gl/crazy-pro/insurance-app/branch/main/graph/badge.svg?token=4FCIMCOFNA)](https://codecov.io/gl/crazy-pro/insurance-app)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=crazy-pr0_insurance-app&metric=coverage)](https://sonarcloud.io/summary/new_code?id=crazy-pr0_insurance-app)

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-black.svg)](https://sonarcloud.io/summary/new_code?id=crazy-pr0_insurance-app)

[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=crazy-pr0_insurance-app&metric=ncloc)](https://sonarcloud.io/summary/new_code?id=crazy-pr0_insurance-app)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=crazy-pr0_insurance-app&metric=duplicated_lines_density)](https://sonarcloud.io/summary/new_code?id=crazy-pr0_insurance-app)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=crazy-pr0_insurance-app&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=crazy-pr0_insurance-app)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=crazy-pr0_insurance-app&metric=bugs)](https://sonarcloud.io/summary/new_code?id=crazy-pr0_insurance-app)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=crazy-pr0_insurance-app&metric=vulnerabilities)](https://sonarcloud.io/summary/new_code?id=crazy-pr0_insurance-app)

[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=crazy-pr0_insurance-app&metric=security_rating)](https://sonarcloud.io/summary/new_code?id=crazy-pr0_insurance-app)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=crazy-pr0_insurance-app&metric=reliability_rating)](https://sonarcloud.io/summary/new_code?id=crazy-pr0_insurance-app)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=crazy-pr0_insurance-app&metric=sqale_rating)](https://sonarcloud.io/summary/new_code?id=crazy-pr0_insurance-app)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=crazy-pr0_insurance-app&metric=sqale_index)](https://sonarcloud.io/summary/new_code?id=crazy-pr0_insurance-app)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=crazy-pr0_insurance-app&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=crazy-pr0_insurance-app)
