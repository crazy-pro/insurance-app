import {createRouter, createWebHistory} from 'vue-router'

import LoginPage from "@/pages/login/LoginPage.vue";
import HomePage from "@/pages/home/HomePage.vue";
import ProfilePage from "@/pages/ProfilePage.vue";

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {path: '/login', name: 'login', component: LoginPage},
        {path: '/profile', name: 'profile', component: ProfilePage},
        {path: '/home', name: 'home', component: HomePage},
        {path: '/', redirect: '/login'}
    ]
})

export default router
