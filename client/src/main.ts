import './styles/main.css'
import SimpleVueValidation from "simple-vue-validator";
import { createPinia } from 'pinia';
import axios from 'axios'
import VueAxios from 'vue-axios'

import {createApp} from 'vue'
import App from './App.vue'
import router from './router'

import { initializeApp } from "firebase/app";
import firebaseConfig from './firebase/firebaseConfig'

initializeApp(firebaseConfig)
const app = createApp(App);

app.use(router)
app.use(SimpleVueValidation)
app.use(createPinia())
app.use(VueAxios, axios)
app.mount('#app')
