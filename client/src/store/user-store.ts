import {defineStore} from 'pinia'

import {Auth, getAuth, signInWithEmailAndPassword, signOut, UserCredential} from "firebase/auth";
import axios from "axios";

interface IUserState {
    id: number | null,
    email: string | null,
    firstName: string | null,
    token: string | null,
    errMsg: string
}

export const useUserStore = defineStore('user', {
    state: (): IUserState => ({
        id: null,
        email: null,
        firstName: null,
        token: null,
        errMsg: "",
    }),
    getters: {
        getId: state => state.id,
        getToken: state => state.token,
        getFirstName: state => state.firstName,
        isLoggedIn: state => !!state.token,
        getErrMsg: state => state.errMsg
    },
    actions: {

        async login(email: string, password: string): Promise<void> {
            const auth: Auth = getAuth();
            try {
                let userCred: UserCredential = await signInWithEmailAndPassword(auth, email, password);
                this.token = userCred.user.accessToken;
                this.email = userCred.user.email;
                await this.fetchUserByEmail();

            } catch (error: any) {
                await this.setErrorMessage(error.code)
            }
        },

        async logout(): Promise<void> {
            const auth: Auth = getAuth();
            try {
                await signOut(auth);
                this.clearUser();

            } catch (error: any) {
                console.log("logout error", error)
            }
        },

        async fetchUserByEmail(): Promise<void> {
            try {
                axios
                    .get(`http://localhost:8080/api/owners/email/?email=${this.email}`)
                    .then(response => {
                        this.id = response.data.id;
                        this.firstName = response.data.firstName;
                    });
            } catch (error) {
                if (error) throw error
            }
        },

        setErrorMessage(errCode: string): void {
            switch (errCode) {
                case "":
                    this.errMsg = "";
                    break;
                case "auth/invalid-email":
                    this.errMsg = "Invalid email";
                    break;
                case "auth/user-not-found":
                    this.errMsg = "No account with this email was found";
                    break;
                case "auth/wrong-password":
                    this.errMsg = "Incorrect password";
                    break;
                case "auth/weak-password":
                    this.errMsg = "Password should be at least 6 characters";
                    break;
                default:
                    this.errMsg = "Email or password was incorrect";
                    break;
            }
        },

        clearUser(): void {
            this.id = null;
            this.email = null;
            this.firstName = null;
            this.token = null;
            this.errMsg = "";
        }
    },
})

