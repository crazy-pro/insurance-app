import {getActivePinia} from 'pinia'
import {watch} from "vue";

export function usePiniaLocalStorage(key: string): void {
    const pinia = getActivePinia()

    if (pinia) {
        // Apply local storage app state
        const existingAppState = localStorage.getItem(key)
        if (existingAppState) {
            const existingValue = JSON.parse(existingAppState)
            pinia.state.value = {instanceKey: existingValue.instanceKey}
        }

        watch(
            () => pinia.state.value,
            (newState): void => {
                localStorage.setItem(key, JSON.stringify(newState))
            },
            {deep: true}
        )
    }
}