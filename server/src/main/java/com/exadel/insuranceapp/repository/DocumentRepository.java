package com.exadel.insuranceapp.repository;

import com.exadel.insuranceapp.entity.Document;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentRepository extends CrudRepository<Document, Long> {

    List<Document> findAllByInsuranceObjectId(Long id);

}
