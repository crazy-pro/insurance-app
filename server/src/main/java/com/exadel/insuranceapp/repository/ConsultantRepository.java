package com.exadel.insuranceapp.repository;

import com.exadel.insuranceapp.entity.Consultant;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConsultantRepository extends CrudRepository<Consultant, Long> {

    List<Consultant> findAllByInsuranceCompanyId(Long id);

    List<Consultant> findByLastName(String lastName);

    Consultant findByEmail(String lastName);
}
