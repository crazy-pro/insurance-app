package com.exadel.insuranceapp.repository;

import com.exadel.insuranceapp.entity.InsuranceObject;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InsuranceObjectRepository extends CrudRepository<InsuranceObject, Long> {

    List<InsuranceObject> findAllByOwnerId(Long id);

}
