package com.exadel.insuranceapp.repository;

import com.exadel.insuranceapp.entity.Plan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlanRepository extends CrudRepository<Plan, Long> {

    List<Plan> findAllByInsuranceCompanyId(Long id);
}
