package com.exadel.insuranceapp.repository;

import com.exadel.insuranceapp.entity.Photo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhotoRepository extends CrudRepository<Photo, Long> {

    List<Photo> findAllByDocumentId(Long id);

}
