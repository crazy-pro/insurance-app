package com.exadel.insuranceapp.repository;

import com.exadel.insuranceapp.entity.Owner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OwnerRepository extends CrudRepository<Owner, Long> {

    List<Owner> findByLastName(String lastName);

    Owner findByEmail(String email);
}
