package com.exadel.insuranceapp.repository;

import com.exadel.insuranceapp.entity.Claim;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClaimRepository extends CrudRepository<Claim, Long> {

    List<Claim> findAllByInsuranceObjectId(Long id);
}
