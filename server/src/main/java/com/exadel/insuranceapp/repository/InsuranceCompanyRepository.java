package com.exadel.insuranceapp.repository;

import com.exadel.insuranceapp.entity.InsuranceCompany;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InsuranceCompanyRepository extends CrudRepository<InsuranceCompany, Long> {
}
