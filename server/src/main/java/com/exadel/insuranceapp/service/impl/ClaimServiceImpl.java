package com.exadel.insuranceapp.service.impl;

import com.exadel.insuranceapp.dto.ClaimDto;
import com.exadel.insuranceapp.entity.Claim;
import com.exadel.insuranceapp.exception.NotFoundException;
import com.exadel.insuranceapp.mapper.ClaimMapper;
import com.exadel.insuranceapp.repository.ClaimRepository;
import com.exadel.insuranceapp.service.ClaimService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class ClaimServiceImpl implements ClaimService {

    private final ClaimRepository claimRepository;
    private final ClaimMapper claimMapper;

    public ClaimServiceImpl(ClaimRepository claimRepository, ClaimMapper claimMapper) {
        this.claimRepository = claimRepository;
        this.claimMapper = claimMapper;
    }

    @Transactional(readOnly = true)
    @Override
    public List<ClaimDto> findAllByInsuranceObjectId(Long id) {
        log.info("Get claims with object id={}", id);
         return claimRepository.findAllByInsuranceObjectId(id).stream()
                .map(claimMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public List<ClaimDto> findAll() {
        log.info("Get all claims");
        return StreamSupport.stream(claimRepository.findAll().spliterator(), false).map(claimMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public ClaimDto findById(Long id) {
        Optional<Claim> result = claimRepository.findById(id);
        if (result.isPresent()) {
            log.info("Get claim with id ={}", id);
            Claim claim = result.get();
            return claimMapper.toDto(claim);
        }
        log.error("Did not find claim id ={}", id);
        throw new NotFoundException("Did not find claim id - " + id);
    }

    @Transactional
    public ClaimDto save(ClaimDto claimDto) {
        log.info("Save claim");
        Claim claim = claimMapper.toEntity(claimDto);
        return claimMapper.toDto(claimRepository.save(claim));
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        if (!claimRepository.existsById(id)) {
            log.info("Did not find claim id ={}", id);
            throw new NotFoundException("Did not find claim id - " + id);
        }
        log.info("Remove claim with id ={}", id);
        claimRepository.deleteById(id);
    }
}
