package com.exadel.insuranceapp.service;

import com.exadel.insuranceapp.dto.ConsultantDto;

import java.util.List;

public interface ConsultantService extends CrudService<ConsultantDto, Long> {

    List<ConsultantDto> findAllByInsuranceCompanyId(Long id);

    List<ConsultantDto> findByLastName(String lastName);
}
