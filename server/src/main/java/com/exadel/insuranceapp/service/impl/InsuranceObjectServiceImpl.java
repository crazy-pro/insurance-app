package com.exadel.insuranceapp.service.impl;

import com.exadel.insuranceapp.dto.InsuranceObjectDto;
import com.exadel.insuranceapp.entity.InsuranceObject;
import com.exadel.insuranceapp.exception.NotFoundException;
import com.exadel.insuranceapp.mapper.InsuranceObjectMapper;
import com.exadel.insuranceapp.repository.InsuranceObjectRepository;
import com.exadel.insuranceapp.service.InsuranceObjectService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class InsuranceObjectServiceImpl implements InsuranceObjectService {

    private final InsuranceObjectRepository insuranceObjectRepository;
    private final InsuranceObjectMapper insuranceObjectMapper;

    public InsuranceObjectServiceImpl(InsuranceObjectRepository insuranceObjectRepository, InsuranceObjectMapper insuranceObjectMapper) {
        this.insuranceObjectRepository = insuranceObjectRepository;
        this.insuranceObjectMapper = insuranceObjectMapper;
    }

    @Transactional(readOnly = true)
    @Override
    public List<InsuranceObjectDto> findAllByOwnerId(Long id) {
        log.info("Get insurance objects with owner id ={}", id);
        return insuranceObjectRepository.findAllByOwnerId(id).stream()
                .map(insuranceObjectMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public List<InsuranceObjectDto> findAll() {
        log.info("Get all insurance objects");
        return StreamSupport.stream(insuranceObjectRepository.findAll().spliterator(), false)
                .map(insuranceObjectMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public InsuranceObjectDto findById(Long id) {
        Optional<InsuranceObject> result = insuranceObjectRepository.findById(id);
        if (result.isPresent()) {
            log.info("Get insurance object with id ={}", id);
            InsuranceObject insuranceObject = result.get();
            return insuranceObjectMapper.toDto(insuranceObject);
        }
        log.info("Did not find insurance object id ={}", id);
        throw new NotFoundException("Did not find insurance object id - " + id);
    }

    @Transactional
    public InsuranceObjectDto save(InsuranceObjectDto insuranceObjectDto) {
        log.info("Save insurance object");
        InsuranceObject insuranceObject = insuranceObjectMapper.toEntity(insuranceObjectDto);
        return insuranceObjectMapper.toDto(insuranceObjectRepository.save(insuranceObject));
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        if (!insuranceObjectRepository.existsById(id)) {
            log.info("Did not find insurance object id ={}", id);
            throw new NotFoundException("Did not find insurance object id - " + id);
        }
        log.info("Remove insurance object with id ={}", id);
        insuranceObjectRepository.deleteById(id);
    }
}
