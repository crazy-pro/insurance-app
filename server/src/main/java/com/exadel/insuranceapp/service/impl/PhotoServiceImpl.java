package com.exadel.insuranceapp.service.impl;

import com.exadel.insuranceapp.dto.PhotoDto;
import com.exadel.insuranceapp.entity.Photo;
import com.exadel.insuranceapp.exception.NotFoundException;
import com.exadel.insuranceapp.mapper.PhotoMapper;
import com.exadel.insuranceapp.repository.PhotoRepository;
import com.exadel.insuranceapp.service.PhotoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class PhotoServiceImpl implements PhotoService {

    private final PhotoRepository photoRepository;
    private final PhotoMapper photoMapper;

    public PhotoServiceImpl(PhotoRepository photoRepository, PhotoMapper photoMapper) {
        this.photoRepository = photoRepository;
        this.photoMapper = photoMapper;
    }

    @Transactional(readOnly = true)
    @Override
    public List<PhotoDto> findAllByDocumentId(Long id) {
        log.info("Get photos with document id ={}", id);
        return photoRepository.findAllByDocumentId(id).stream()
                .map(photoMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public List<PhotoDto> findAll() {
        log.info("Get all photos");
        return StreamSupport
                .stream(photoRepository.findAll().spliterator(), false)
                .map(photoMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public PhotoDto findById(Long id) {
        Optional<Photo> result = photoRepository.findById(id);
        if (result.isPresent()) {
            log.info("Get photo with id ={}", id);
            Photo photo = result.get();
            return photoMapper.toDto(photo);
        }
        log.info("Did not find photo id ={}", id);
        throw new NotFoundException("Did not find photo id - " + id);
    }

    @Transactional
    public PhotoDto save(PhotoDto photoDto) {
        log.info("Save photo");
        Photo photo = photoMapper.toEntity(photoDto);
        return photoMapper.toDto(photoRepository.save(photo));
    }

    @Transactional
    public void deleteById(Long id) {
        if (!photoRepository.existsById(id)) {
            log.info("Did not find photo id ={}", id);
            throw new NotFoundException("Did not find photo id - " + id);
        }
        log.info("Remove photo with id ={}", id);
        photoRepository.deleteById(id);
    }
}
