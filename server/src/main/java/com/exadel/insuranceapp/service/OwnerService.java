package com.exadel.insuranceapp.service;

import com.exadel.insuranceapp.dto.OwnerDto;

import java.util.List;

public interface OwnerService extends CrudService<OwnerDto, Long>{

    List<OwnerDto> findByLastName(String lastName);

    OwnerDto findByEmail(String email);
}
