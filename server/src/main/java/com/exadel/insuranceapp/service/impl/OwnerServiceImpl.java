package com.exadel.insuranceapp.service.impl;

import com.exadel.insuranceapp.dto.OwnerDto;
import com.exadel.insuranceapp.entity.Owner;
import com.exadel.insuranceapp.exception.NotFoundException;
import com.exadel.insuranceapp.mapper.OwnerMapper;
import com.exadel.insuranceapp.repository.OwnerRepository;
import com.exadel.insuranceapp.service.OwnerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class OwnerServiceImpl implements OwnerService {

    private final OwnerRepository ownerRepository;
    private final OwnerMapper ownerMapper;

    public OwnerServiceImpl(OwnerRepository ownerRepository, OwnerMapper ownerMapper) {
        this.ownerRepository = ownerRepository;
        this.ownerMapper = ownerMapper;
    }

    @Transactional(readOnly = true)
    @Override
    public OwnerDto findByEmail(String email) {
        log.info("Get owner with email ={}", email);
        return ownerMapper.toDto(ownerRepository.findByEmail(email));
    }

    @Transactional(readOnly = true)
    @Override
    public List<OwnerDto> findByLastName(String lastName) {
        log.info("Get owners with last name ={}", lastName);
        return ownerRepository.findByLastName(lastName).stream()
                .map(ownerMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public List<OwnerDto> findAll() {
        log.info("Get all owners");
        return StreamSupport
                .stream(ownerRepository.findAll().spliterator(), false)
                .map(ownerMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public OwnerDto findById(Long id) {
        Optional<Owner> result = ownerRepository.findById(id);
        if (result.isPresent()) {
            log.info("Get owner with id ={}", id);
            Owner owner = result.get();
            return ownerMapper.toDto(owner);
        }
        log.info("Did not find owner id ={}", id);
        throw new NotFoundException("Did not find owner id - " + id);
    }

    @Transactional
    @Override
    public OwnerDto save(OwnerDto ownerDto) {
        log.info("Save owner");
        Owner owner = ownerMapper.toEntity(ownerDto);
        return ownerMapper.toDto(ownerRepository.save(owner));
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        if (!ownerRepository.existsById(id)) {
            log.info("Did not find owner id ={}", id);
            throw new NotFoundException("Did not find owner id - " + id);
        }
        log.info("Remove owner with id ={}", id);
        ownerRepository.deleteById(id);
    }
}
