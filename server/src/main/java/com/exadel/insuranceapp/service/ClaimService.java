package com.exadel.insuranceapp.service;

import com.exadel.insuranceapp.dto.ClaimDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ClaimService extends CrudService<ClaimDto, Long> {

    @Transactional(readOnly = true)
    List<ClaimDto> findAllByInsuranceObjectId(Long id);
}
