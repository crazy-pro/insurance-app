package com.exadel.insuranceapp.service;

import com.exadel.insuranceapp.dto.ContractDto;

public interface ContractService extends CrudService<ContractDto, Long> {
}