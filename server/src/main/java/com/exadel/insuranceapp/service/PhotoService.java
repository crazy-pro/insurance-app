package com.exadel.insuranceapp.service;

import com.exadel.insuranceapp.dto.PhotoDto;

import java.util.List;

public interface PhotoService extends CrudService<PhotoDto, Long> {

    List<PhotoDto> findAllByDocumentId(Long id);
}
