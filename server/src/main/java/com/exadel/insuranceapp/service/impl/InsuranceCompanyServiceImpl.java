package com.exadel.insuranceapp.service.impl;

import com.exadel.insuranceapp.dto.InsuranceCompanyDto;
import com.exadel.insuranceapp.entity.InsuranceCompany;
import com.exadel.insuranceapp.exception.NotFoundException;
import com.exadel.insuranceapp.mapper.InsuranceCompanyMapper;
import com.exadel.insuranceapp.repository.InsuranceCompanyRepository;
import com.exadel.insuranceapp.service.InsuranceCompanyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class InsuranceCompanyServiceImpl implements InsuranceCompanyService {

    private final InsuranceCompanyRepository insuranceCompanyRepository;
    private final InsuranceCompanyMapper insuranceCompanyMapper;

    public InsuranceCompanyServiceImpl(InsuranceCompanyRepository insuranceCompanyRepository, InsuranceCompanyMapper insuranceCompanyMapper) {
        this.insuranceCompanyRepository = insuranceCompanyRepository;
        this.insuranceCompanyMapper = insuranceCompanyMapper;
    }

    @Transactional(readOnly = true)
    @Override
    public List<InsuranceCompanyDto> findAll() {
        log.info("Get all insurance companies");
        return StreamSupport
                .stream(insuranceCompanyRepository.findAll().spliterator(), false)
                .map(insuranceCompanyMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public InsuranceCompanyDto findById(Long id) {
        Optional<InsuranceCompany> result = insuranceCompanyRepository.findById(id);
        if (result.isPresent()) {
            log.info("Get insurance company with id ={}", id);
            InsuranceCompany insuranceCompany = result.get();
            return insuranceCompanyMapper.toDto(insuranceCompany);
        }
        log.info("Did not find insurance company id ={}", id);
        throw new NotFoundException("Did not find insurance company id - " + id);
    }

    @Transactional
    public InsuranceCompanyDto save(InsuranceCompanyDto insuranceCompanyDto) {
        log.info("Save insurance company");
        InsuranceCompany insuranceCompany = insuranceCompanyMapper.toEntity(insuranceCompanyDto);
        return insuranceCompanyMapper.toDto(insuranceCompanyRepository.save(insuranceCompany));
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        if (!insuranceCompanyRepository.existsById(id)) {
            log.error("Did not find insurance company id ={}", id);
            throw new NotFoundException("Did not find insurance company id - " + id);
        }
        log.info("Remove insurance company with id ={}", id);
        insuranceCompanyRepository.deleteById(id);
    }
}
