package com.exadel.insuranceapp.service;

import com.exadel.insuranceapp.dto.InsuranceCompanyDto;

public interface InsuranceCompanyService extends CrudService<InsuranceCompanyDto, Long> {
}
