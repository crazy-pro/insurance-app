package com.exadel.insuranceapp.service;

import com.exadel.insuranceapp.dto.InsuranceObjectDto;

import java.util.List;

public interface InsuranceObjectService extends CrudService<InsuranceObjectDto, Long> {
    List<InsuranceObjectDto> findAllByOwnerId(Long id);
}
