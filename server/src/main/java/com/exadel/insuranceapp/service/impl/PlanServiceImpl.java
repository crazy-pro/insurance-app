package com.exadel.insuranceapp.service.impl;

import com.exadel.insuranceapp.dto.PlanDto;
import com.exadel.insuranceapp.entity.Plan;
import com.exadel.insuranceapp.exception.NotFoundException;
import com.exadel.insuranceapp.mapper.PlanMapper;
import com.exadel.insuranceapp.repository.PlanRepository;
import com.exadel.insuranceapp.service.PlanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class PlanServiceImpl implements PlanService {

    private final PlanRepository planRepository;
    private final PlanMapper planMapper;

    public PlanServiceImpl(PlanRepository planRepository, PlanMapper planMapper) {
        this.planRepository = planRepository;
        this.planMapper = planMapper;
    }

    @Transactional(readOnly = true)
    @Override
    public List<PlanDto> findAllByInsuranceCompanyId(Long id) {
        log.info("Get plans with insurance company id ={}", id);
        return planRepository.findAllByInsuranceCompanyId(id).stream()
                .map(planMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public List<PlanDto> findAll() {
        log.info("Get all plans");
        return StreamSupport.stream(planRepository.findAll().spliterator(), false).map(planMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public PlanDto findById(Long id) {
        Optional<Plan> result = planRepository.findById(id);
        if (result.isPresent()) {
            log.info("Get plan with id ={}", id);
            Plan plan = result.get();
            return planMapper.toDto(plan);
        }
        log.info("Did not find plan id ={}", id);
        throw new NotFoundException("Did not find plan id - " + id);
    }

    @Transactional
    public PlanDto save(PlanDto planDto) {
        log.info("Save plan");
        Plan plan = planMapper.toEntity(planDto);
        return planMapper.toDto(planRepository.save(plan));
    }

    @Transactional
    public void deleteById(Long id) {
        if (!planRepository.existsById(id)) {
            log.info("Did not find plan id ={}", id);
            throw new NotFoundException("Did not find plan id - " + id);
        }
        log.info("Remove plan with id ={}", id);
        planRepository.deleteById(id);
    }
}
