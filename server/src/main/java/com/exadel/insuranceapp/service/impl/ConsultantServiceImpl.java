package com.exadel.insuranceapp.service.impl;

import com.exadel.insuranceapp.dto.ConsultantDto;
import com.exadel.insuranceapp.entity.Consultant;
import com.exadel.insuranceapp.exception.NotFoundException;
import com.exadel.insuranceapp.mapper.ConsultantMapper;
import com.exadel.insuranceapp.repository.ConsultantRepository;
import com.exadel.insuranceapp.service.ConsultantService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class ConsultantServiceImpl  implements ConsultantService {

    private final ConsultantRepository consultantRepository;
    private final ConsultantMapper consultantMapper;

    public ConsultantServiceImpl(ConsultantRepository consultantRepository, ConsultantMapper consultantMapper) {
        this.consultantRepository = consultantRepository;
        this.consultantMapper = consultantMapper;
    }

    @Transactional(readOnly = true)
    @Override
    public List<ConsultantDto> findAllByInsuranceCompanyId(Long id) {
        log.info("Get consultants with insurance company id ={}", id);
        return consultantRepository.findAllByInsuranceCompanyId(id).stream()
                .map(consultantMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public List<ConsultantDto> findByLastName(String lastName) {
        log.info("Get consultants with last name ={}", lastName);
        return consultantRepository.findByLastName(lastName).stream()
                .map(consultantMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public List<ConsultantDto> findAll() {
        log.info("Get all consultants");
        return StreamSupport.stream(consultantRepository.findAll().spliterator(), false)
                .map(consultantMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public ConsultantDto findById(Long id) {
        Optional<Consultant> result = consultantRepository.findById(id);
        if (result.isPresent()) {
            log.info("Get consultant with id ={}", id);
            Consultant consultant = result.get();
            return consultantMapper.toDto(consultant);
        }
        log.info("Did not find consultant id ={}", id);
        throw new NotFoundException("Did not find consultant id - " + id);
    }

    @Transactional
    public ConsultantDto save(ConsultantDto consultantDto) {
        log.info("Save consultant");
        Consultant consultant = consultantMapper.toEntity(consultantDto);
        return consultantMapper.toDto(consultantRepository.save(consultant));
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        if (!consultantRepository.existsById(id)) {
            log.info("Did not find consultant id ={}", id);
            throw new NotFoundException("Did not find consultant id - " + id);
        }
        log.info("Delete consultant with id ={}", id);
        consultantRepository.deleteById(id);
    }
}
