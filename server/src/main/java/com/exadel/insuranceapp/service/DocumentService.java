package com.exadel.insuranceapp.service;

import com.exadel.insuranceapp.dto.DocumentDto;

import java.util.List;

public interface DocumentService extends CrudService<DocumentDto, Long> {

    List<DocumentDto> findAllByInsuranceObjectId(Long id);
}
