package com.exadel.insuranceapp.service.impl;

import com.exadel.insuranceapp.dto.DocumentDto;
import com.exadel.insuranceapp.entity.Document;
import com.exadel.insuranceapp.exception.NotFoundException;
import com.exadel.insuranceapp.mapper.DocumentMapper;
import com.exadel.insuranceapp.repository.DocumentRepository;
import com.exadel.insuranceapp.service.DocumentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class DocumentServiceImpl implements DocumentService {

    private final DocumentRepository documentRepository;
    private final DocumentMapper documentMapper;

    public DocumentServiceImpl(DocumentRepository documentRepository, DocumentMapper documentMapper) {
        this.documentRepository = documentRepository;
        this.documentMapper = documentMapper;
    }

    @Transactional(readOnly = true)
    @Override
    public List<DocumentDto> findAllByInsuranceObjectId(Long id) {
        log.info("Get documents with insurance object id ={}", id);
        return documentRepository.findAllByInsuranceObjectId(id).stream()
                .map(documentMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public List<DocumentDto> findAll() {
        log.info("Get all documents");
        return StreamSupport
                .stream(documentRepository.findAll().spliterator(), false)
                .map(documentMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public DocumentDto findById(Long id) {
        Optional<Document> result = documentRepository.findById(id);
        if (result.isPresent()) {
            log.info("Get document with id ={}", id);
            Document document = result.get();
            return documentMapper.toDto(document);
        }
        log.info("Did not find document id ={}", id);
        throw new NotFoundException("Did not find document id - " + id);

    }

    @Transactional
    public DocumentDto save(DocumentDto documentDto) {
        Document document = documentMapper.toEntity(documentDto);
        return documentMapper.toDto(documentRepository.save(document));
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        if (!documentRepository.existsById(id)) {
            log.info("Did not find document id ={}", id);
            throw new NotFoundException("Did not find document id - " + id);
        }
        log.info("Delete document with id ={}", id);
        documentRepository.deleteById(id);
    }
}
