package com.exadel.insuranceapp.service;

import com.exadel.insuranceapp.dto.PlanDto;

import java.util.List;

public interface PlanService extends CrudService<PlanDto, Long> {

    List<PlanDto> findAllByInsuranceCompanyId(Long id);
}
