package com.exadel.insuranceapp.service.impl;

import com.exadel.insuranceapp.dto.ContractDto;
import com.exadel.insuranceapp.entity.Contract;
import com.exadel.insuranceapp.exception.NotFoundException;
import com.exadel.insuranceapp.mapper.ContractMapper;
import com.exadel.insuranceapp.repository.ContractRepository;
import com.exadel.insuranceapp.service.ContractService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class ContractServiceImpl implements ContractService {

    private final ContractRepository contractRepository;
    private final ContractMapper contractMapper;

    public ContractServiceImpl(ContractRepository contractRepository, ContractMapper contractMapper) {
        this.contractRepository = contractRepository;
        this.contractMapper = contractMapper;
    }

    @Transactional(readOnly = true)
    @Override
    public List<ContractDto> findAll() {
        log.info("Get all contracts");
        return StreamSupport
                .stream(contractRepository.findAll().spliterator(), false)
                .map(contractMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public ContractDto findById(Long id) {
        Optional<Contract> result = contractRepository.findById(id);
        if (result.isPresent()) {
            log.info("Get contract with id ={}", id);
            Contract contract = result.get();
            return contractMapper.toDto(contract);
        }
        log.info("Did not find contract id ={}", id);
        throw new NotFoundException("Did not find contract id - " + id);
    }

    @Transactional
    public ContractDto save(ContractDto contractDto) {
        log.info("Save contract");
        Contract contract = contractMapper.toEntity(contractDto);
        return contractMapper.toDto(contractRepository.save(contract));
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        if (!contractRepository.existsById(id)) {
            log.info("Did not find contract id ={}", id);
            throw new NotFoundException("Did not find contract id - " + id);
        }
        log.info("Remove contract with id ={}", id);
        contractRepository.deleteById(id);
    }
}
