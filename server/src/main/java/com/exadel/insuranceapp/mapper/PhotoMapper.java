package com.exadel.insuranceapp.mapper;

import com.exadel.insuranceapp.dto.PhotoDto;
import com.exadel.insuranceapp.entity.Photo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface PhotoMapper {

    PhotoMapper INSTANCE = Mappers.getMapper(PhotoMapper.class);

    PhotoDto toDto(Photo photo);

    Photo toEntity(PhotoDto photoDto);
}