package com.exadel.insuranceapp.mapper;

import com.exadel.insuranceapp.dto.ConsultantDto;
import com.exadel.insuranceapp.entity.Consultant;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ConsultantMapper {

    ConsultantMapper INSTANCE = Mappers.getMapper(ConsultantMapper.class);

    ConsultantDto toDto(Consultant consultant);

    Consultant toEntity(ConsultantDto consultantDto);
}
