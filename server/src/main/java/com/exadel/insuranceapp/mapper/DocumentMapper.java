package com.exadel.insuranceapp.mapper;

import com.exadel.insuranceapp.dto.DocumentDto;
import com.exadel.insuranceapp.entity.Document;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface DocumentMapper {

    DocumentMapper INSTANCE = Mappers.getMapper(DocumentMapper.class);

    DocumentDto toDto(Document Document);

    Document toEntity(DocumentDto documentDto);
}