package com.exadel.insuranceapp.mapper;

import com.exadel.insuranceapp.dto.OwnerDto;
import com.exadel.insuranceapp.entity.Owner;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface OwnerMapper {

    OwnerMapper INSTANCE = Mappers.getMapper(OwnerMapper.class);

    OwnerDto toDto(Owner owner);

    Owner toEntity(OwnerDto ownerDto);
}