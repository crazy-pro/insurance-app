package com.exadel.insuranceapp.mapper;

import com.exadel.insuranceapp.dto.ClaimDto;
import com.exadel.insuranceapp.entity.Claim;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ClaimMapper {

    ClaimMapper INSTANCE = Mappers.getMapper(ClaimMapper.class);

    ClaimDto toDto(Claim claim);

    Claim toEntity(ClaimDto claimDto);
}
