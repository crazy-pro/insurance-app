package com.exadel.insuranceapp.mapper;

import com.exadel.insuranceapp.dto.PlanDto;
import com.exadel.insuranceapp.entity.Plan;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface PlanMapper {

    PlanMapper INSTANCE = Mappers.getMapper(PlanMapper.class);

    PlanDto toDto(Plan plan);

    Plan toEntity(PlanDto planDto);
}
