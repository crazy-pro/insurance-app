package com.exadel.insuranceapp.mapper;

import com.exadel.insuranceapp.dto.InsuranceCompanyDto;
import com.exadel.insuranceapp.entity.InsuranceCompany;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface InsuranceCompanyMapper {

    InsuranceCompanyMapper INSTANCE = Mappers.getMapper(InsuranceCompanyMapper.class);

    InsuranceCompanyDto toDto(InsuranceCompany insuranceCompany);

    InsuranceCompany toEntity(InsuranceCompanyDto insuranceCompanyDto);
}

