package com.exadel.insuranceapp.mapper;

import com.exadel.insuranceapp.dto.InsuranceObjectDto;
import com.exadel.insuranceapp.entity.InsuranceObject;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface InsuranceObjectMapper {

    InsuranceObjectMapper INSTANCE = Mappers.getMapper(InsuranceObjectMapper.class);

   InsuranceObjectDto toDto(InsuranceObject insuranceObject);

   InsuranceObject toEntity(InsuranceObjectDto insuranceObjectDto);
}

