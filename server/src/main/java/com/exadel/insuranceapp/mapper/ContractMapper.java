package com.exadel.insuranceapp.mapper;

import com.exadel.insuranceapp.dto.ContractDto;
import com.exadel.insuranceapp.entity.Contract;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ContractMapper {

    ContractMapper INSTANCE = Mappers.getMapper(ContractMapper.class);

    ContractDto toDto(Contract contract);

    Contract toEntity(ContractDto contractDto);
}