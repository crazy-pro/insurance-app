package com.exadel.insuranceapp.controller;

import com.exadel.insuranceapp.dto.OwnerDto;
import com.exadel.insuranceapp.service.OwnerService;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/owners")
public class OwnerController {

    private final OwnerService ownerService;

    public OwnerController(OwnerService ownerService) {
        this.ownerService = ownerService;
    }

    @GetMapping
    public List<OwnerDto> getOwners() {
        return ownerService.findAll();
    }

    @GetMapping("/{ownerId}")
    public OwnerDto getOwnerById(@PathVariable Long ownerId) {
        return ownerService.findById(ownerId);
    }

    @GetMapping("/lastName/")
    public List<OwnerDto> getOwnerByLastName(@RequestParam(value = "lastName") String lastName) {
        return ownerService.findByLastName(lastName);
    }

    @GetMapping("/email/")
    public OwnerDto getOwnerByEmail(@RequestParam(value = "email") String email) {
        return ownerService.findByEmail(email);
    }

    @PostMapping
    public OwnerDto saveOwner(@RequestBody @Valid OwnerDto ownerDto) {
        return ownerService.save(ownerDto);
    }

    @PutMapping
    public OwnerDto updateOwner(@RequestBody @Valid OwnerDto ownerDto) {
        return ownerService.save(ownerDto);
    }

    @DeleteMapping("/{ownerId}")
    public void deleteOwnerById(@PathVariable Long ownerId) {
        ownerService.deleteById(ownerId);
    }
}
