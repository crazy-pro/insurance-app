package com.exadel.insuranceapp.controller;

import com.exadel.insuranceapp.dto.PlanDto;
import com.exadel.insuranceapp.service.PlanService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/plans")
public class PlanController {

    private final PlanService planService;

    public PlanController(PlanService planService) {
        this.planService = planService;
    }

    @GetMapping
    public List<PlanDto> getPlans() {
        return planService.findAll();
    }

    @GetMapping("/{planId}")
    public PlanDto getPlanById(@PathVariable Long planId) {
        return planService.findById(planId);
    }

    @GetMapping("/companies/{companyId}")
    public List<PlanDto> getPlansByCompanyId(@PathVariable Long companyId) {
        return planService.findAllByInsuranceCompanyId(companyId);
    }

    @PostMapping
    public PlanDto savePlan(@RequestBody PlanDto planDto) {
        return planService.save(planDto);
    }

    @PutMapping
    public PlanDto updatePlan(@RequestBody PlanDto planDto) {
        return planService.save(planDto);
    }

    @DeleteMapping("/{planId}")
    public void deletePlanById(@PathVariable Long planId) {
        planService.deleteById(planId);
    }
}

