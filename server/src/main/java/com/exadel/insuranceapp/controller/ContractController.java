package com.exadel.insuranceapp.controller;

import com.exadel.insuranceapp.dto.ContractDto;
import com.exadel.insuranceapp.service.ContractService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/contracts")
public class ContractController {

    private final ContractService contractService;

    public ContractController(ContractService contractService) {
        this.contractService = contractService;
    }

    @GetMapping
    public List<ContractDto> getContracts() {
        return contractService.findAll();
    }

    @GetMapping("/{contractId}")
    public ContractDto getContractById(@PathVariable Long contractId) {
        return contractService.findById(contractId);
    }

    @PostMapping
    public ContractDto saveContract(@RequestBody ContractDto contractDto) {
        return contractService.save(contractDto);
    }

    @PutMapping
    public ContractDto updateContract(@RequestBody ContractDto contractDto) {
        return contractService.save(contractDto);
    }

    @DeleteMapping("/{contractId}")
    public void deleteContractById(@PathVariable Long contractId) {
        contractService.deleteById(contractId);
    }
}
