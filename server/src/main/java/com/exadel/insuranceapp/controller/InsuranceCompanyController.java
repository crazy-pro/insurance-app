package com.exadel.insuranceapp.controller;

import com.exadel.insuranceapp.dto.InsuranceCompanyDto;
import com.exadel.insuranceapp.service.InsuranceCompanyService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/companies")
public class InsuranceCompanyController {

    private final InsuranceCompanyService insuranceCompanyService;

    public InsuranceCompanyController(InsuranceCompanyService insuranceCompanyService) {
        this.insuranceCompanyService = insuranceCompanyService;
    }

    @GetMapping
    public List<InsuranceCompanyDto> getCompanies() {
        return insuranceCompanyService.findAll();
    }

    @GetMapping("/{companyId}")
    public InsuranceCompanyDto getCompanyById(@PathVariable Long companyId) {
        return insuranceCompanyService.findById(companyId);
    }

    @PostMapping
    public InsuranceCompanyDto saveCompany(@RequestBody InsuranceCompanyDto companyDto) {
        return insuranceCompanyService.save(companyDto);
    }

    @PutMapping
    public InsuranceCompanyDto updateCompany(@RequestBody InsuranceCompanyDto companyDto) {
        return insuranceCompanyService.save(companyDto);
    }

    @DeleteMapping("/{companyId}")
    public void deleteCompanyById(@PathVariable Long companyId) {
        insuranceCompanyService.deleteById(companyId);
    }
}
