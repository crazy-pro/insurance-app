package com.exadel.insuranceapp.controller;

import com.exadel.insuranceapp.dto.DocumentDto;
import com.exadel.insuranceapp.service.DocumentService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/documents")
public class DocumentController {

    private final DocumentService documentService;

    public DocumentController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @GetMapping
    public List<DocumentDto> getDocuments() {
        return documentService.findAll();
    }

    @GetMapping("/{documentId}")
    public DocumentDto getDocumentById(@PathVariable Long documentId) {
        return documentService.findById(documentId);
    }

    @GetMapping("/objects/{objectId}")
    public List<DocumentDto> getDocumentsByInsuranceObjectId(@PathVariable Long objectId) {
        return documentService.findAllByInsuranceObjectId(objectId);
    }

    @PostMapping
    public DocumentDto saveDocument(@RequestBody DocumentDto documentDto) {
        return documentService.save(documentDto);
    }

    @PutMapping
    public DocumentDto updateDocument(@RequestBody DocumentDto documentDto) {
        return documentService.save(documentDto);
    }

    @DeleteMapping("/{documentId}")
    public void deleteDocumentById(@PathVariable Long documentId) {
        documentService.deleteById(documentId);
    }
}
