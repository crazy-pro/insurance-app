package com.exadel.insuranceapp.controller;

import com.exadel.insuranceapp.dto.PhotoDto;
import com.exadel.insuranceapp.service.PhotoService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("photos")
public class PhotoController {

    private final PhotoService photoService;

    public PhotoController(PhotoService photoService) {
        this.photoService = photoService;
    }

    @GetMapping
    public List<PhotoDto> getPhotos() {
        return photoService.findAll();
    }

    @GetMapping("/{photoId}")
    public PhotoDto getPhotoById(@PathVariable Long photoId) {
        return photoService.findById(photoId);
    }

    @GetMapping("/documents/{documentId}")
    public List<PhotoDto> getPhotosByDocumentId(@PathVariable Long documentId) {
        return photoService.findAllByDocumentId(documentId);
    }

    @PostMapping
    public PhotoDto savePhoto(@RequestBody PhotoDto photoDto) {
        return photoService.save(photoDto);
    }

    @PutMapping
    public PhotoDto updatePhoto(@RequestBody PhotoDto photoDto) {
        return photoService.save(photoDto);
    }

    @DeleteMapping("/{photoId}")
    public void deletePhotoById(@PathVariable Long photoId) {
        photoService.deleteById(photoId);
    }
}
