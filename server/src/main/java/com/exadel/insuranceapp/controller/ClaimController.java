package com.exadel.insuranceapp.controller;

import com.exadel.insuranceapp.dto.ClaimDto;
import com.exadel.insuranceapp.service.ClaimService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/claims")
public class ClaimController {

    private final ClaimService claimService;

    public ClaimController(ClaimService claimService) {
        this.claimService = claimService;
    }

    @GetMapping
    public List<ClaimDto> getClaims() {
        return claimService.findAll();
    }

    @GetMapping("/{claimId}")
    public ClaimDto getClaimById(@PathVariable Long claimId) {
        return claimService.findById(claimId);
    }

    @GetMapping("/objects/{objectId}")
    public List<ClaimDto> getClaimsByInsuranceObjectId(@PathVariable Long objectId) {
        return claimService.findAllByInsuranceObjectId(objectId);
    }

    @PostMapping
    public ClaimDto saveClaim(@RequestBody ClaimDto claimDto) {
        return claimService.save(claimDto);
    }

    @PutMapping
    public ClaimDto updateClaim(@RequestBody ClaimDto claimDto) {
        return claimService.save(claimDto);
    }

    @DeleteMapping("/{claimId}")
    public void deleteClaimById(@PathVariable Long claimId) {
        claimService.deleteById(claimId);
    }
}
