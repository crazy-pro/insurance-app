package com.exadel.insuranceapp.controller;

import com.exadel.insuranceapp.dto.InsuranceObjectDto;
import com.exadel.insuranceapp.service.InsuranceObjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("objects")
public class InsuranceObjectController {

    private final InsuranceObjectService insuranceObjectService;

    @Autowired
    public InsuranceObjectController(InsuranceObjectService insuranceObjectService) {
        this.insuranceObjectService = insuranceObjectService;
    }

    @GetMapping
    public List<InsuranceObjectDto> getObjects() {
        return insuranceObjectService.findAll();
    }

    @GetMapping("/{objectId}")
    public InsuranceObjectDto getObjectById(@PathVariable Long objectId) {
        return insuranceObjectService.findById(objectId);
    }

    @GetMapping("/owners/{ownerId}")
    public List<InsuranceObjectDto> getObjectsByOwnerId(@PathVariable Long ownerId) {
        return insuranceObjectService.findAllByOwnerId(ownerId);
    }

    @PostMapping
    public InsuranceObjectDto saveObject(@RequestBody InsuranceObjectDto objectDto) {
        return insuranceObjectService.save(objectDto);
    }

    @PutMapping
    public InsuranceObjectDto updateObject(@RequestBody InsuranceObjectDto objectDto) {
        return insuranceObjectService.save(objectDto);
    }

    @DeleteMapping("/{objectId}")
    public void deleteObjectById(@PathVariable Long objectId) {
        insuranceObjectService.deleteById(objectId);
    }
}
