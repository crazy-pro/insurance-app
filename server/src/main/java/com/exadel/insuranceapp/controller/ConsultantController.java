package com.exadel.insuranceapp.controller;

import com.exadel.insuranceapp.dto.ConsultantDto;
import com.exadel.insuranceapp.service.ConsultantService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/consultants")
public class ConsultantController {

    private final ConsultantService consultantService;

    public ConsultantController(ConsultantService consultantService) {
        this.consultantService = consultantService;
    }

    @GetMapping
    public List<ConsultantDto> getConsultants() {
        return consultantService.findAll();
    }

    @GetMapping("/{consultantId}")
    public ConsultantDto getConsultantById(@PathVariable Long consultantId) {
        return consultantService.findById(consultantId);
    }

    @GetMapping("/")
    public List<ConsultantDto> getConsultantsByLastName(@RequestParam(value = "lastName") String lastName) {
        return consultantService.findByLastName(lastName);
    }

    @GetMapping("/companies/{companyId}")
    public List<ConsultantDto> getConsultantsByInsuranceCompanyId(@PathVariable Long companyId) {
        return consultantService.findAllByInsuranceCompanyId(companyId);
    }

    @PostMapping
    public ConsultantDto saveConsultant(@RequestBody ConsultantDto consultantDto) {
        return consultantService.save(consultantDto);
    }

    @PutMapping
    public ConsultantDto updateConsultant(@RequestBody ConsultantDto consultantDto) {
        return consultantService.save(consultantDto);
    }

    @DeleteMapping("/{consultantId}")
    public void deleteConsultantById(@PathVariable Long consultantId) {
        consultantService.deleteById(consultantId);
    }
}
