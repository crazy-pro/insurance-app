package com.exadel.insuranceapp.entity;

import jakarta.persistence.Column;
import lombok.*;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address {
    private String country;
    private String state;
    private String city;
    private String street;

    @Column(name = "home_num")
    private String homeNum;

    @Column(name = "post_code")
    private String postCode;
}
