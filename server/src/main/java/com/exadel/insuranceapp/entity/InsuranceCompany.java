package com.exadel.insuranceapp.entity;


import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name="insurance_companies")
public class InsuranceCompany extends BaseEntity{

    private String name;

    @Embedded
    private Address address;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "insuranceCompany")
    private List<Consultant> consultants;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "insuranceCompany")
    private List<Plan> plans;
}
