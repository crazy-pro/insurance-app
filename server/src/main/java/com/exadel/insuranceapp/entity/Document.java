package com.exadel.insuranceapp.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name="documents")
public class Document extends BaseEntity {

    private String name;

    private BigDecimal amount;

    @Column(name = "claim_issue_date")
    private LocalDateTime claimIssueDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "claim_id")
    private Claim claim;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "insurance_object_id")
    private InsuranceObject insuranceObject;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "document")
    private List<Photo> photos;
}
