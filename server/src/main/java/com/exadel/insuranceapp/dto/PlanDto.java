package com.exadel.insuranceapp.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.Period;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class PlanDto {

    @Setter(AccessLevel.NONE)
    private Long id;

    private BigDecimal price;

    private Period duration;

    private String description;

//    private InsuranceCompanyDto insuranceCompany;
}
