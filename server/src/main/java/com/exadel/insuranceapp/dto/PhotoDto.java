package com.exadel.insuranceapp.dto;

import com.exadel.insuranceapp.entity.Document;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class PhotoDto {

    @Setter(AccessLevel.NONE)
    private Long id;

    private byte[] picture;

    private String name;

    private LocalDateTime createdDateTime;

    private Document document;
}
