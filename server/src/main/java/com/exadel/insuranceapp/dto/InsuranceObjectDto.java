package com.exadel.insuranceapp.dto;

import lombok.AccessLevel;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;
import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class InsuranceObjectDto {

    @Setter(AccessLevel.NONE)
    private Long id;

    private String name;

    private String model;

    private LocalDateTime createdDate;

    private InsuranceCompanyDto insuranceCompany;

    private OwnerDto owner;

    private List<ClaimDto> claim;
}
