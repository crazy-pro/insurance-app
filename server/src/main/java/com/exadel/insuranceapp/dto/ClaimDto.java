package com.exadel.insuranceapp.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class ClaimDto {

    @Setter(AccessLevel.NONE)
    private Long id;

//    private InsuranceObjectDto insuranceObject;

    private ContractDto contract;

    private LocalDateTime createdDate;
}
