package com.exadel.insuranceapp.dto;

import com.exadel.insuranceapp.entity.Address;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class InsuranceCompanyDto {

    @Setter(AccessLevel.NONE)
    private Long id;

    private String name;

    private Address address;

//    private List<ConsultantDto> consultants;

    private List<PlanDto> plans;
}
