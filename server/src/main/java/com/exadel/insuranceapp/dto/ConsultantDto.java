package com.exadel.insuranceapp.dto;

import com.exadel.insuranceapp.entity.Address;
import com.exadel.insuranceapp.entity.enums.Gender;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class ConsultantDto {

    @Setter(AccessLevel.NONE)
    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private int age;

    private Gender gender;

    private String phoneNumber;

    private Address address;

    private List<ContractDto> contracts;

    private InsuranceCompanyDto insuranceCompany;

}
