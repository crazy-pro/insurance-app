package com.exadel.insuranceapp.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class DocumentDto {

    @Setter(AccessLevel.NONE)
    private Long id;

    private String name;

    private BigDecimal amount;

    private LocalDateTime claimIssueDate;

    private InsuranceObjectDto insuranceObject;

    private List<PhotoDto> photos;
}
