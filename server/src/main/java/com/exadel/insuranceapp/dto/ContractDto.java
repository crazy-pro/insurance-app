package com.exadel.insuranceapp.dto;

import com.exadel.insuranceapp.entity.Plan;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;
import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class ContractDto {

    @Setter(AccessLevel.NONE)
    private Long id;

    private Plan plan;

    private InsuranceObjectDto insuranceObjectDto;

    private List<ClaimDto> claims;

    private ConsultantDto consultant;

    private LocalDate startDate;

    private LocalDate endDate;

    private InsuranceCompanyDto insuranceCompany;

}
