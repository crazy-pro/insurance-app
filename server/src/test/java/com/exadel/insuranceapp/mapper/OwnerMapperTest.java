package com.exadel.insuranceapp.mapper;

import com.exadel.insuranceapp.dto.OwnerDto;
import com.exadel.insuranceapp.entity.Owner;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OwnerMapperTest {

    public static final String FIRST_NAME = "Jimmy";
    public static final String LAST_NAME = "Fallon";

    OwnerMapper ownerMapper = OwnerMapper.INSTANCE;

    @Test
    public void testOwnerToOwnerDto() {
        Owner ownerMock = new Owner();
        ownerMock.setFirstName(FIRST_NAME);
        ownerMock.setLastName(LAST_NAME);

        OwnerDto ownerDto = ownerMapper.toDto(ownerMock);

        assertEquals(FIRST_NAME, ownerDto.getFirstName());
        assertEquals(LAST_NAME, ownerDto.getLastName());
    }

}
