package com.exadel.insuranceapp.controller;

import com.exadel.insuranceapp.entity.Consultant;
import com.exadel.insuranceapp.mapper.ConsultantMapper;
import com.exadel.insuranceapp.repository.ConsultantRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ConsultantControllerTest {
    private static final Consultant CONSULTANT = Consultant.builder()
            .firstName("John")
            .lastName("Doe")
            .email("doe@test.com")
            .age(1)
            .build();
    private static final String URL = "/consultants";
    Long id = null;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ConsultantMapper consultantMapper;
    @Autowired
    private ConsultantRepository repository;
    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void createClaimForTest() {
        Consultant CONSULTANT_SAVED = repository.save(CONSULTANT);
        id = CONSULTANT_SAVED.getId();
    }

    @AfterEach
    public void deleteClaimForTest() {
        repository.deleteById(id);
    }

    @Test
    public void testCreateConsultant() throws Exception {
        repository.deleteById(repository.findByEmail(CONSULTANT.getEmail()).getId());

        mockMvc.perform(
                        post(URL)
                                .content(objectMapper.writeValueAsString(CONSULTANT))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber());
        repository.deleteById(repository.findByEmail(CONSULTANT.getEmail()).getId());
    }

    @Test
    public void testUpdateConsultant() throws Exception {
        Consultant UPDATED_CONSULTANT = repository.findById(id).orElseThrow(null);
        UPDATED_CONSULTANT.setContracts(null);

        mockMvc.perform(
                        post(URL)
                                .content(objectMapper.writeValueAsString(UPDATED_CONSULTANT))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id));
    }

    @Test
    public void testGetConsultantById() throws Exception {
        mockMvc.perform(
                        get(URL + "/{id}", id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id));
    }

    @Test
    public void testGetConsultantByLastName() throws Exception {
        String lastName = CONSULTANT.getLastName();
        mockMvc.perform(get(URL + "/")
                        .param("lastName", lastName))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].lastName").value(lastName));
    }

    @Test
    public void testGetConsultants() throws Exception {
        mockMvc.perform(
                        get(URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    public void testGetConsultantsByCompanyId() throws Exception {
        Long companyId = 1L;

        mockMvc.perform(
                        get(URL + "/companies/{companyId}", companyId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    public void testGetConsultantNotFound() throws Exception {
        mockMvc.perform(
                        get(URL + "/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteConsultantById() throws Exception {
        mockMvc.perform(
                        delete(URL + "/{id}", id))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteClaimNotFound() throws Exception {
        mockMvc.perform(
                        delete(URL + "/0"))
                .andExpect(status().isNotFound());
    }
}
