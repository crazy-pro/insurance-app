package com.exadel.insuranceapp.controller;

import com.exadel.insuranceapp.entity.Claim;
import com.exadel.insuranceapp.repository.ClaimRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ClaimControllerTest {
    private static final Claim CLAIM = Claim.builder().build();
    private static final String URL = "/claims";
    private Long id = null;

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private ClaimRepository repository;
    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void createClaimForTest() {
        id = repository.save(CLAIM).getId();
    }

    @AfterEach
    public void deleteClaimForTest() {
        repository.deleteById(id);
    }

    @Test
    public void testCreateClaim() throws Exception {
        mockMvc.perform(
                        post(URL)
                                .content(objectMapper.writeValueAsString(CLAIM))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber());
    }

    @Test
    public void testUpdateClaim() throws Exception {
        CLAIM.setId(id);
        mockMvc.perform(
                        put(URL)
                                .content(objectMapper.writeValueAsString(CLAIM))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id));
    }

    @Test
    public void testGetClaimById() throws Exception {

        mockMvc.perform(
                        get(URL + "/{id}", id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id));
    }

    @Test
    public void testGetClaims() throws Exception {
        mockMvc.perform(
                        get(URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    public void testGetClaimsByObjectId() throws Exception {
        Long objectId = 1L;
        mockMvc.perform(
                        get(URL + "/objects/{objectId}", objectId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    public void testGetClaimNotFound() throws Exception {
        mockMvc.perform(
                        get(URL + "/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteClaimById() throws Exception {
        mockMvc.perform(
                        delete(URL + "/{id}", id))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteClaimNotFound() throws Exception {
        mockMvc.perform(
                        delete(URL + "/0"))
                .andExpect(status().isNotFound());
    }
}
