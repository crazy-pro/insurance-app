package com.exadel.insuranceapp.service;

import com.exadel.insuranceapp.dto.DocumentDto;
import com.exadel.insuranceapp.entity.Document;
import com.exadel.insuranceapp.entity.InsuranceObject;
import com.exadel.insuranceapp.exception.NotFoundException;
import com.exadel.insuranceapp.mapper.DocumentMapper;
import com.exadel.insuranceapp.repository.DocumentRepository;
import com.exadel.insuranceapp.service.impl.DocumentServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class DocumentServiceTest {
    private static final long DOCUMENT_ID = 1L;

    @Mock
    DocumentRepository documentRepository;

    DocumentMapper documentMapper = DocumentMapper.INSTANCE;

    DocumentService documentService;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);

        documentService = new DocumentServiceImpl(documentRepository, documentMapper);
    }

    @Test
    public void testFindAll() {
        List<Document> documentMock = new ArrayList<>();
        documentMock.add(Document.builder().build());
        documentMock.add(Document.builder().build());
        documentMock.add(Document.builder().build());
        documentMock.add(Document.builder().build());

        when(documentRepository.findAll()).thenReturn(documentMock);

        List<DocumentDto> documents = documentService.findAll();

        assertEquals(documentMock.size(), documents.size());
    }

    @Test
    public void testFindById() {
        Document documentMock = Document.builder().id(DOCUMENT_ID).build();

        when(documentRepository.findById(anyLong())).thenReturn(Optional.of(documentMock));

        DocumentDto documentFound = documentService.findById(DOCUMENT_ID);

        assertEquals(DOCUMENT_ID, documentFound.getId());
    }

    @Test
    public void testFindByIdNotFound() {
        assertThrows(NotFoundException.class, () -> documentService.findById(DOCUMENT_ID));
    }

    @Test
    public void testSave() {
        DocumentDto documentMockDto = DocumentDto.builder().build();
        Document document = Document.builder().build();

        when(documentRepository.save(any())).thenReturn(document);

        DocumentDto documentSaved = documentService.save(documentMockDto);

        assertNotNull(documentSaved);
        verify(documentRepository).save(any());
    }

    @Test
    public void testDeleteById() {
        when(documentRepository.existsById(DOCUMENT_ID)).thenReturn(true);
        if (documentRepository.existsById(DOCUMENT_ID)) {
            documentService.deleteById(DOCUMENT_ID);
            verify(documentRepository, times(1))
                    .deleteById(DOCUMENT_ID);
        }
    }

    @Test
    public void testDeleteByIdNotFound() {
        assertThrows(NotFoundException.class, () -> documentService.deleteById(DOCUMENT_ID));
    }

    @Test
    public void testFindAllByInsuranceObjectId() {
        long INS_OBJ_ID = 1L;

        List<Document> documentsMock = new ArrayList<>();
        documentsMock.add(Document.builder().insuranceObject(InsuranceObject.builder().id(INS_OBJ_ID).build()).build());
        documentsMock.add(Document.builder().insuranceObject(InsuranceObject.builder().id(INS_OBJ_ID).build()).build());
        documentsMock.add(Document.builder().insuranceObject(InsuranceObject.builder().id(INS_OBJ_ID).build()).build());
        documentsMock.add(Document.builder().insuranceObject(InsuranceObject.builder().id(INS_OBJ_ID).build()).build());

        when(documentRepository.findAllByInsuranceObjectId(anyLong())).thenReturn(documentsMock);

        List<DocumentDto> claims = documentService.findAllByInsuranceObjectId(INS_OBJ_ID);

        assertEquals(documentsMock.size(), claims.size());
    }
}
