package com.exadel.insuranceapp.service;

import com.exadel.insuranceapp.dto.ClaimDto;
import com.exadel.insuranceapp.entity.Claim;
import com.exadel.insuranceapp.entity.InsuranceObject;
import com.exadel.insuranceapp.exception.NotFoundException;
import com.exadel.insuranceapp.mapper.ClaimMapper;
import com.exadel.insuranceapp.repository.ClaimRepository;
import com.exadel.insuranceapp.service.impl.ClaimServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ClaimServiceTest {
    private static final long CLAIM_ID = 1L;

    @Mock
    ClaimRepository claimRepository;

    ClaimMapper claimMapper = ClaimMapper.INSTANCE;

    ClaimService claimService;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);

        claimService = new ClaimServiceImpl(claimRepository, claimMapper);
    }

    @Test
    public void testFindAll() {
        InsuranceObject object = InsuranceObject.builder().name("name").build();
        List<Claim> claimsMock = new ArrayList<>();
        claimsMock.add(Claim.builder().insuranceObject(object).build());
        claimsMock.add(Claim.builder().insuranceObject(object).build());
        claimsMock.add(Claim.builder().insuranceObject(object).build());
        claimsMock.add(Claim.builder().insuranceObject(object).build());

        when(claimRepository.findAll()).thenReturn(claimsMock);

        List<ClaimDto> claims = claimService.findAll();

        assertEquals(claimsMock.size(), claims.size());
    }

    @Test
    public void testFindById() {
        Claim claimMock = Claim.builder().id(CLAIM_ID).build();

        when(claimRepository.findById(anyLong())).thenReturn(Optional.ofNullable(claimMock));

        ClaimDto claimFound = claimService.findById(CLAIM_ID);

        assertEquals(CLAIM_ID, claimFound.getId());

    }

    @Test
    public void testFindByIdNotFound() {
        assertThrows(NotFoundException.class, () -> claimService.findById(CLAIM_ID));
    }

    @Test
    public void testSave() {
        ClaimDto claimMockDto = ClaimDto.builder().build();
        Claim claim = Claim.builder().build();

        when(claimRepository.save(any())).thenReturn(claim);

        ClaimDto claimSaved = claimService.save(claimMockDto);

        assertNotNull(claimSaved);

        verify(claimRepository).save(any());
    }

    @Test
    public void testDeleteById() {
        when(claimRepository.existsById(CLAIM_ID)).thenReturn(true);

        if (claimRepository.existsById(CLAIM_ID)) {
            claimService.deleteById(CLAIM_ID);
            verify(claimRepository, times(1))
                    .deleteById(CLAIM_ID);
        }
    }

    @Test
    public void testDeleteByIdNotFound() {
        assertThrows(NotFoundException.class, () -> claimService.deleteById(CLAIM_ID));
    }

    @Test
    public void testFindAllByInsuranceObjectId() {
        long INS_OBJ_ID = 1L;
        List<Claim> claimsMock = new ArrayList<>();
        claimsMock.add(Claim.builder().insuranceObject(InsuranceObject.builder().id(INS_OBJ_ID).build()).build());
        claimsMock.add(Claim.builder().insuranceObject(InsuranceObject.builder().id(INS_OBJ_ID).build()).build());
        claimsMock.add(Claim.builder().insuranceObject(InsuranceObject.builder().id(INS_OBJ_ID).build()).build());
        claimsMock.add(Claim.builder().insuranceObject(InsuranceObject.builder().id(INS_OBJ_ID).build()).build());

        when(claimRepository.findAllByInsuranceObjectId(anyLong())).thenReturn(claimsMock);

        List<ClaimDto> claims = claimService.findAllByInsuranceObjectId(INS_OBJ_ID);

        assertEquals(claimsMock.size(), claims.size());
    }
}
