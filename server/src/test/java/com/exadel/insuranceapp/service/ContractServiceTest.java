package com.exadel.insuranceapp.service;

import com.exadel.insuranceapp.dto.ContractDto;
import com.exadel.insuranceapp.entity.Contract;
import com.exadel.insuranceapp.exception.NotFoundException;
import com.exadel.insuranceapp.mapper.ContractMapper;
import com.exadel.insuranceapp.repository.ContractRepository;
import com.exadel.insuranceapp.service.impl.ContractServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ContractServiceTest {
    private static final long CONTRACT_ID = 1L;

    @Mock
    ContractRepository contractRepository;

    ContractMapper contractMapper = ContractMapper.INSTANCE;

    ContractService contractService;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);

        contractService = new ContractServiceImpl(contractRepository, contractMapper);
    }

    @Test
    public void testFindAll() {
        List<Contract> contractsMock = new ArrayList<>();
        contractsMock.add(Contract.builder().build());
        contractsMock.add(Contract.builder().build());
        contractsMock.add(Contract.builder().build());
        contractsMock.add(Contract.builder().build());

        when(contractRepository.findAll()).thenReturn(contractsMock);

        List<ContractDto> contracts = contractService.findAll();

        assertEquals(contractsMock.size(), contracts.size());
    }

    @Test
    public void testFindById() {
        Contract contractMock = Contract.builder().id(CONTRACT_ID).build();

        when(contractRepository.findById(anyLong())).thenReturn(Optional.of(contractMock));

        ContractDto contactFound = contractService.findById(CONTRACT_ID);

        assertEquals(CONTRACT_ID, contactFound.getId());
    }

    @Test
    public void testFindByIdNotFound() {
        assertThrows(NotFoundException.class, () -> contractService.findById(CONTRACT_ID));
    }

    @Test
    public void testSave() {
        ContractDto contractMockDto = ContractDto.builder().build();
        Contract contract = Contract.builder().build();

        when(contractRepository.save(any())).thenReturn(contract);

        ContractDto contractSaved = contractService.save(contractMockDto);

        assertNotNull(contractSaved);

        verify(contractRepository).save(any());
    }

    @Test
    public void testDeleteById() {
        when(contractRepository.existsById(CONTRACT_ID)).thenReturn(true);

        if (contractRepository.existsById(CONTRACT_ID)) {
            contractService.deleteById(CONTRACT_ID);
            verify(contractRepository, times(1))
                    .deleteById(CONTRACT_ID);
        }
    }

    @Test
    public void testDeleteByIdNotFound() {
        assertThrows(NotFoundException.class, () -> contractService.deleteById(CONTRACT_ID));
    }
}
