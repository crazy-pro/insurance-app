package com.exadel.insuranceapp.service;

import com.exadel.insuranceapp.dto.OwnerDto;
import com.exadel.insuranceapp.entity.Owner;
import com.exadel.insuranceapp.exception.NotFoundException;
import com.exadel.insuranceapp.mapper.OwnerMapper;
import com.exadel.insuranceapp.repository.OwnerRepository;
import com.exadel.insuranceapp.service.impl.OwnerServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OwnerServiceTest {
    private static final long OWNER_ID = 1L;

    @Mock
    OwnerRepository ownerRepository;

    OwnerMapper ownerMapper = OwnerMapper.INSTANCE;

    OwnerService ownerService;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);

        ownerService = new OwnerServiceImpl(ownerRepository, ownerMapper);
    }


    @Test
    public void testFindAll() {
        List<Owner> ownersMock = new ArrayList<>();
        ownersMock.add(Owner.builder().id(1L).firstName("John").lastName("Wick").age(1).email("wick1@test.com").build());
        ownersMock.add(Owner.builder().id(2L).firstName("John").lastName("Wick").age(2).email("wick2@test.com").build());
        ownersMock.add(Owner.builder().id(3L).firstName("John").lastName("Wick").age(3).email("wick3@test.com").build());
        ownersMock.add(Owner.builder().id(4L).firstName("John").lastName("Wick").age(4).email("wick4@test.com").build());

        when(ownerRepository.findAll()).thenReturn(ownersMock);

        List<OwnerDto> owners = ownerService.findAll();

        assertEquals(ownersMock.size(), owners.size());
    }

    @Test
    public void testFindById() {
        Owner ownerMock = Owner.builder().id(OWNER_ID).firstName("John").lastName("Wick").age(1).email("wick1@test.com").build();

        when(ownerRepository.findById(anyLong())).thenReturn(Optional.of(ownerMock));

        OwnerDto ownerFound = ownerService.findById(OWNER_ID);

        assertEquals(OWNER_ID, ownerFound.getId());
    }

    @Test
    public void testFindByIdNotFound() {
        assertThrows(NotFoundException.class, () -> ownerService.findById(OWNER_ID));
    }

    @Test
    public void testSave() {
        OwnerDto ownerMockDto = OwnerDto.builder().firstName("John").lastName("Wick").age(1).email("wick1@test.com").build();
        Owner owner = Owner.builder()
                .firstName(ownerMockDto
                        .getFirstName())
                .lastName(ownerMockDto.getLastName())
                .age(ownerMockDto.getAge()).build();

        when(ownerRepository.save(any())).thenReturn(owner);

        OwnerDto ownerSaved = ownerService.save(ownerMockDto);

        assertEquals(ownerMockDto.getFirstName(), ownerSaved.getFirstName());

        assertNotNull(ownerSaved);

        verify(ownerRepository).save(any());
    }

    @Test
    public void testDeleteById() {
        when(ownerRepository.existsById(OWNER_ID)).thenReturn(true);

        if (ownerRepository.existsById(OWNER_ID)) {
            ownerService.deleteById(OWNER_ID);
            verify(ownerRepository, times(1))
                    .deleteById(OWNER_ID);
        }
    }

    @Test
    public void testDeleteByIdNotFound() {
        assertThrows(NotFoundException.class, () -> ownerService.deleteById(OWNER_ID));
    }

    @Test
    public void testFindByLastName() {
        String LAST_NAME = "Wick";

        List<Owner> ownersMock = new ArrayList<>();
        ownersMock.add(Owner.builder().id(1L).firstName("John").lastName("Wick").age(1).email("wick1@test.com").build());
        ownersMock.add(Owner.builder().id(2L).firstName("John").lastName("Wick").age(2).email("wick2@test.com").build());
        ownersMock.add(Owner.builder().id(3L).firstName("John").lastName("Wick").age(3).email("wick3@test.com").build());
        ownersMock.add(Owner.builder().id(4L).firstName("John").lastName("Wick").age(4).email("wick4@test.com").build());

        when(ownerRepository.findByLastName(anyString())).thenReturn(ownersMock);

        List<OwnerDto> owners = ownerService.findByLastName(LAST_NAME);

        assertEquals(ownersMock.size(), owners.size());
    }
}
