package com.exadel.insuranceapp.service;

import com.exadel.insuranceapp.dto.ConsultantDto;
import com.exadel.insuranceapp.entity.Consultant;
import com.exadel.insuranceapp.entity.InsuranceCompany;
import com.exadel.insuranceapp.exception.NotFoundException;
import com.exadel.insuranceapp.mapper.ConsultantMapper;
import com.exadel.insuranceapp.repository.ConsultantRepository;
import com.exadel.insuranceapp.service.impl.ConsultantServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ConsultantServiceTest {
    private static final long CONSULTANT_ID = 1L;

    @Mock
    ConsultantRepository consultantRepository;

    ConsultantMapper consultantMapper = ConsultantMapper.INSTANCE;

    ConsultantService consultantService;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);

        consultantService = new ConsultantServiceImpl(consultantRepository, consultantMapper);
    }

    @Test
    public void testFindAll() {
        List<Consultant> consultantsMock = new ArrayList<>();
        consultantsMock.add(Consultant.builder().id(1L).firstName("John").lastName("Wick").age(1).email("wick1@test.com").build());
        consultantsMock.add(Consultant.builder().id(1L).firstName("John").lastName("Wick").age(2).email("wick2@test.com").build());
        consultantsMock.add(Consultant.builder().id(1L).firstName("John").lastName("Wick").age(3).email("wick2@test.com").build());
        consultantsMock.add(Consultant.builder().id(1L).firstName("John").lastName("Wick").age(4).email("wick3@test.com").build());

        when(consultantRepository.findAll()).thenReturn(consultantsMock);

        List<ConsultantDto> consultants = consultantService.findAll();

        assertEquals(consultantsMock.size(), consultants.size());
    }

    @Test
    public void testFindById() {
        Consultant consultantMock = Consultant.builder().id(CONSULTANT_ID).firstName("John").lastName("Wick").age(1).email("wick1@test.com").build();

        when(consultantRepository.findById(anyLong())).thenReturn(Optional.of(consultantMock));

        ConsultantDto consultantFound = consultantService.findById(CONSULTANT_ID);

        assertEquals(CONSULTANT_ID, consultantFound.getId());
    }

    @Test
    public void testFindByIdNotFound() {
        assertThrows(NotFoundException.class, () -> consultantService.findById(CONSULTANT_ID));
    }

    @Test
    public void testSave() {
        ConsultantDto consultantMockDto = ConsultantDto.builder()
                .firstName("John")
                .lastName("Wick")
                .age(1)
                .email("wick1@test.com")
                .build();
        Consultant consultant = Consultant.builder()
                .firstName(consultantMockDto.getFirstName())
                .lastName(consultantMockDto.getLastName())
                .age(consultantMockDto.getAge())
                .email(consultantMockDto.getEmail())
                .build();
        when(consultantRepository.save(any())).thenReturn(consultant);

        ConsultantDto consultantSaved = consultantService.save(consultantMockDto);

        assertNotNull(consultantSaved);

        assertEquals(consultantMockDto.getFirstName(), consultantSaved.getFirstName());

        verify(consultantRepository).save(any());
    }

    @Test
    public void testDeleteById() {
        when(consultantRepository.existsById(CONSULTANT_ID)).thenReturn(true);

        if (consultantRepository.existsById(CONSULTANT_ID)) {
            consultantService.deleteById(CONSULTANT_ID);
            verify(consultantRepository, times(1))
                    .deleteById(CONSULTANT_ID);
        }
    }

    @Test
    public void testDeleteByIdNotFound() {
        assertThrows(NotFoundException.class, () -> consultantService.deleteById(CONSULTANT_ID));
    }

    @Test
    public void testFindByLastName() {
        String LAST_NAME = "Wick";

        List<Consultant> consultantsMock = new ArrayList<>();
        consultantsMock.add(Consultant.builder().id(1L).firstName("John").lastName("Wick").age(1).email("wick1@test.com").build());
        consultantsMock.add(Consultant.builder().id(2L).firstName("John").lastName("Wick").age(2).email("wick2@test.com").build());
        consultantsMock.add(Consultant.builder().id(3L).firstName("John").lastName("Wick").age(3).email("wick3@test.com").build());
        consultantsMock.add(Consultant.builder().id(4L).firstName("John").lastName("Wick").age(4).email("wick4@test.com").build());

        when(consultantRepository.findByLastName(LAST_NAME)).thenReturn(consultantsMock);

        List<ConsultantDto> consultants = consultantService.findByLastName(LAST_NAME);

        assertEquals(consultantsMock.size(), consultants.size());
    }

    @Test
    public void testFindAllByInsuranceCompanyId() {
        long INS_COMPANY_ID = 1L;
        List<Consultant> consultantsMock = new ArrayList<>();
        consultantsMock.add(Consultant.builder().insuranceCompany(InsuranceCompany.builder().id(INS_COMPANY_ID).build()).age(1).build());
        consultantsMock.add(Consultant.builder().insuranceCompany(InsuranceCompany.builder().id(INS_COMPANY_ID).build()).age(2).build());
        consultantsMock.add(Consultant.builder().insuranceCompany(InsuranceCompany.builder().id(INS_COMPANY_ID).build()).age(3).build());
        consultantsMock.add(Consultant.builder().insuranceCompany(InsuranceCompany.builder().id(INS_COMPANY_ID).build()).age(4).build());

        when(consultantRepository.findAllByInsuranceCompanyId(anyLong())).thenReturn(consultantsMock);

        List<ConsultantDto> consultants = consultantService.findAllByInsuranceCompanyId(INS_COMPANY_ID);

        assertEquals(consultantsMock.size(), consultants.size());
    }
}
