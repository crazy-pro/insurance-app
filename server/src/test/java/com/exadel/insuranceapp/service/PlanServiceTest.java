package com.exadel.insuranceapp.service;

import com.exadel.insuranceapp.dto.PlanDto;
import com.exadel.insuranceapp.entity.InsuranceCompany;
import com.exadel.insuranceapp.entity.Plan;
import com.exadel.insuranceapp.exception.NotFoundException;
import com.exadel.insuranceapp.mapper.PlanMapper;
import com.exadel.insuranceapp.repository.PlanRepository;
import com.exadel.insuranceapp.service.impl.PlanServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PlanServiceTest {
    private static final long PLAN_ID = 1L;

    @Mock
    PlanRepository planRepository;

    PlanMapper planMapper = PlanMapper.INSTANCE;

    PlanService planService;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);

        planService = new PlanServiceImpl(planRepository, planMapper);
    }

    @Test
    public void testFindAll() {
        List<Plan> plansMock = new ArrayList<>();
        plansMock.add(Plan.builder().build());
        plansMock.add(Plan.builder().build());

        when(planRepository.findAll()).thenReturn(plansMock);

        List<PlanDto> plans = planService.findAll();

        assertEquals(plansMock.size(), plans.size());
    }

    @Test
    public void testFindById() {
        Plan planMock = Plan.builder().id(PLAN_ID).build();

        when(planRepository.findById(anyLong())).thenReturn(Optional.of(planMock));

        PlanDto planFound = planService.findById(PLAN_ID);

        assertEquals(PLAN_ID, planFound.getId());
    }

    @Test
    public void testFindByIdNotFound() {
        assertThrows(NotFoundException.class, () -> planService.findById(PLAN_ID));
    }

    @Test
    public void testSave() {
        PlanDto planMockDto = PlanDto.builder().build();
        Plan plan = Plan.builder().build();

        when(planRepository.save(any())).thenReturn(plan);

        PlanDto planSaved = planService.save(planMockDto);

        assertNotNull(planSaved);
        verify(planRepository).save(any());
    }

    @Test
    public void testDeleteById() {
        when(planRepository.existsById(PLAN_ID)).thenReturn(true);

        if (planRepository.existsById(PLAN_ID)) {
            planService.deleteById(PLAN_ID);
            verify(planRepository, times(1))
                    .deleteById(PLAN_ID);
        }
    }

    @Test
    public void testDeleteByIdNotFound() {
        assertThrows(NotFoundException.class, () -> planService.deleteById(PLAN_ID));
    }

    @Test
    public void testFindAllByInsuranceCompanyId() {
        long INS_COMPANY_ID = 1L;

        List<Plan> planMock = new ArrayList<>();
        planMock.add(Plan.builder().insuranceCompany(InsuranceCompany.builder().id(INS_COMPANY_ID).build()).build());
        planMock.add(Plan.builder().insuranceCompany(InsuranceCompany.builder().id(INS_COMPANY_ID).build()).build());
        planMock.add(Plan.builder().insuranceCompany(InsuranceCompany.builder().id(INS_COMPANY_ID).build()).build());

        when(planRepository.findAllByInsuranceCompanyId(anyLong())).thenReturn(planMock);

        List<PlanDto> plans = planService.findAllByInsuranceCompanyId(INS_COMPANY_ID);

        assertEquals(planMock.size(), plans.size());
    }
}
